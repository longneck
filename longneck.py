#!/usr/bin/python

import pygame
from optparse import OptionParser
import os.path

parser = OptionParser()

parser.add_option("-x", "--width",
                    dest="width",
                    type='int',
                    default=500,
                    help="internal width of the new image in pixels (minimal: 330)")

parser.add_option("-y", "--height",
                    dest="height",
                    type='int',
                    default=500,
                    help="internal height of the new image in pixels (minimal: 370)")

parser.add_option("-w", "--scalewidth",
                    dest="scalewidth",
                    type='int',
                    help="final width of the new image in pixels")

parser.add_option("-g", "--scaleheight",
                    dest="scaleheight",
                    type='int',
                    help="final height of the new image in pixels")

parser.add_option("-c", "--scale",
                    dest="scale",
                    type='float',
                    help="scale factor for the output")

parser.add_option("-f", "--file",
                    dest="file",
                    default="longneck.png",
                    help="write to this file, default: longneck.png")

parser.add_option("-p", "--params",
                    dest="params",
                    action="store_true",
                    help="out used parameters int the output filename")

(options, args) = parser.parse_args()

width, height = max(330, options.width), max(370, options.height)

suffix = "x%sy%s" % (width, height)

head = pygame.image.load(os.path.join('data', 'head.png'))
desk = pygame.image.load(os.path.join('data', 'desk.png'))

target = pygame.Surface((width, height))
target.fill(pygame.Color('white'))
target.blit(head, head.get_rect())
desk_rect = desk.get_rect()
desk_rect.right = width
desk_rect.bottom = height
target.blit(desk, desk_rect)

start = (112,124)
end = (width - 200, height - 111)
line_width = 5

pygame.draw.line(target, pygame.Color('black'), start, end, line_width)

if options.scale:
    out_surface = pygame.transform.smoothscale(target, (width * options.scale, height * options.scale))
    suffix += "s%f" % options.scale
elif options.scaleheight and options.scalewidth:
    out_surface = pygame.transform.smoothscale(target, (options.scalewidth, options.scaleheight))
    suffix += "w%sg%s" % (options.scalewidth, options.scaleheight)
else:
    out_surface = target

parts = options.file.split('.')
if options.params:
    options.file = '.'.join(parts[:-1] + [suffix, parts[-1]])

pygame.image.save(out_surface, options.file)
